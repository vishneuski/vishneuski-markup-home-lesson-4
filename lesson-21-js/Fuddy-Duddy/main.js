'use strict';

var start = +prompt('Enter the starting number: ');
var final = +prompt('Enter the final number: ');

function fuddyDuddy(a, b) {
  for (var i = a; i <= b; i++) {

    if (i % 3 === 0 && i % 5 === 0) {
      console.log(i + ' - Fuddy-Duddy!!!');
      continue;
    }

    console.log(i);
  }
}

fuddyDuddy(start, final);
